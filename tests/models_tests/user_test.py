from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

    def test_get_attribute(self):
        assert self.chuck.get_attribute("LOCATION") == "EUROPE"
        assert self.justin.get_attribute("LOCATION") == "USA"

    def test_delete_multi(self):
        self.elvis.add_multi("PHONE", '+14086445543')
        assert self.elvis.get_multi("PHONE") == ['+14086445543', '+14086551234']
        self.elvis.delete_multi("PHONE", '+14086445543')
        assert self.elvis.get_multi("PHONE") == [ '+14086551234']
