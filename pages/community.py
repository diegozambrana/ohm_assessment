import six

from flask import jsonify, render_template, request, Response
from functions import app
from models import User
from alembic import op
from sqlalchemy import text
from models._helpers import db

if six.PY3:
    from flask_login import current_user, login_user
else:
    from flask.ext.login import current_user, login_user

@app.route('/community', methods=['GET'])
def community():

    sql = text('''
        SELECT 
            u.user_id, u.display_name, u.tier,
            u.point_balance, rum.attribute, ru.attribute
        FROM user AS u 
        INNER JOIN rel_user_multi AS rum
              ON u.user_id=rum.user_id 
        INNER JOIN rel_user AS ru
              ON u.user_id=ru.user_id
        WHERE 
            rum.rel_lookup="PHONE" AND
            ru.rel_lookup="LOCATION"
        ''')
    users = db.engine.execute(sql)
    users = users.fetchall()
    list_users = [{'id':row[0],
                   'display_name':row[1],
                   'tier':row[2],
                   'point_balance':row[3],
                   'phone': row[4],
                   'location': row[5]} for row in users]
    args = {'users': list_users}
    return render_template("community.html", **args)


