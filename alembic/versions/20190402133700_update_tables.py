"""update tables

Revision ID: 3a0410b8fa6d
Revises: 00000000
Create Date: 2019-04-02 13:37:00.130179

"""

# revision identifiers, used by Alembic.
revision = '3a0410b8fa6d'
down_revision = '00000000'

from alembic import op
import sqlalchemy as sa
from models.user import User

def upgrade():
    op.execute('''UPDATE user SET point_balance = 5000 WHERE user_id=1''')
    op.execute('''UPDATE user SET tier = "Silver" WHERE user_id=3''')
    op.execute('''INSERT INTO rel_user (user_id, rel_lookup, attribute)
        VALUES
            (2, 'LOCATION', 'USA')
    ''')


def downgrade():
    op.execute('''UPDATE user SET point_balance = 0 WHERE user_id=1''')
    op.execute('''UPDATE user SET tier = "Carbon" WHERE user_id=3''')
    op.execute('''DELETE rel_user WHERE user_id = 2''')
